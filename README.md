# ExtraLaf

A binding for more Swing look and feels to make GroIMP even more beautiful 


currently:
- Darcula by Konstantin Bulenkov (https://github.com/bulenkov/Darcula).
- severak FlatLaf by JFormDesigner (https://github.com/JFormDesigner/FlatLaf/tree/main)



## Adding to GroIMP

This plugin does not compile and can just be pushed as a folder into the plugin directory.
Afterwards the theme can be selected in the GroIMP properties. 
